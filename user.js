// ==UserScript==
// @name     Codeberg/Forgejo Ignore
// @version  3
// @grant    none
// @match https://codeberg.org/*/*/issues/*
// @match https://codeberg.org/*/*/pulls/*
// @match https://codeberg.org/
// ==/UserScript==

// Add users you want to ignore to this array.
const muted_users = [ "wojtekxtx" ]

window.onload = function() {
	setTimeout(function() {
		muted_users.forEach(user => {
			document.querySelectorAll('.timeline-item').forEach(div => {
				div.querySelectorAll('.timeline-avatar[href="/' + user.toString() + '"]') .forEach(child => {
					div.style.display = 'none';
				})
			})
			document.querySelectorAll('.news').forEach(news => {
				news.querySelectorAll('.left').forEach(left => {
					left.querySelectorAll('.avatar[title="' + user.toString() + '"]') .forEach(child => {
						news.style.display = 'none';
					})
				})
			})
		})
	}, 200);
};
